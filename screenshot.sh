#!/usr/bin/env bash

datenow=$(date +%H_%M_%Y-%m-%d)
counter=1

while [ -e "/home/hamza/pix/screenshots/screenshot_${datenow}_${counter}.png" ]; do
        counter=$((counter+1))
done

output="/home/hamza/pix/screenshots/screenshot_${datenow}_${counter}.png"

# This will change the output to remove the _1 on the first image
[ -e "/home/hamza/pix/screenshots/screenshot_${datenow}.png" ] \
    || output="/home/hamza/pix/screenshots/screenshot_${datenow}.png"

import -silent -window root "$output"
